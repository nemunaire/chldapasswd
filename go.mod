module git.nemunai.re/chldapasswd

go 1.22

require (
	github.com/amoghe/go-crypt v0.0.0-20220222110647-20eada5f5964
	github.com/go-ldap/ldap/v3 v3.4.10
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)

require (
	github.com/Azure/go-ntlmssp v0.0.0-20221128193559-754e69321358 // indirect
	github.com/go-asn1-ber/asn1-ber v1.5.7 // indirect
	github.com/google/uuid v1.6.0 // indirect
	golang.org/x/crypto v0.31.0 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
)
