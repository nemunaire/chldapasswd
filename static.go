package main

import (
	"embed"
	"html/template"
	"log"
	"net/http"
)

//go:embed all:static
var assets embed.FS

func displayTmpl(w http.ResponseWriter, page string, vars map[string]interface{}) {
	data, err := assets.ReadFile("static/" + page)
	if err != nil {
		log.Fatalf("Unable to find %q: %s", page, err.Error())
	}

	footer, err := assets.ReadFile("static/footer.html")
	if err != nil {
		log.Fatalf("Unable to find %q: %s", "footer.html", err.Error())
	}

	header, err := assets.ReadFile("static/header.html")
	if err != nil {
		log.Fatalf("Unable to find %q: %s", "header.html", err.Error())
	}

	tpl := template.Must(template.New("page").Parse(string(data)))
	tpl.New("footer.html").Parse(string(footer))
	tpl.New("header.html").Parse(string(header))
	tpl.ExecuteTemplate(w, "page", vars)
}

func displayTmplError(w http.ResponseWriter, statusCode int, page string, vars map[string]interface{}) {
	w.WriteHeader(statusCode)
	displayTmpl(w, page, vars)
}

func displayMsg(w http.ResponseWriter, msg string, statusCode int) {
	w.WriteHeader(statusCode)

	label := "error"
	if statusCode < 400 {
		label = "message"
	}

	displayTmpl(w, "message.html", map[string]interface{}{label: msg})
}
