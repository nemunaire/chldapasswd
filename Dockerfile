FROM golang:1 as gobuild

RUN apt-get update && \
    apt-get install -y build-essential git

WORKDIR /go/src/git.nemunai.re/chldapasswd

ADD . .

RUN go get -d && \
    go build -ldflags="-s -w"


FROM debian:stable-slim

RUN apt-get update && \
    apt-get install -y ca-certificates && \
    rm -rf /var/lib/apt/lists/*

EXPOSE 8080

USER nobody

WORKDIR /srv

ENTRYPOINT ["/srv/chldapasswd", "-bind=:8080"]

COPY --from=gobuild /go/src/git.nemunai.re/chldapasswd/chldapasswd /srv/chldapasswd
