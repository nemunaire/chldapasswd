package main

import (
	"errors"
	"log"
	"net/http"
)

func checkPasswdConstraint(password string) error {
	if len(password) < 8 {
		return errors.New("too short, please choose a password at least 8 characters long.")
	}

	return nil
}

func changePassword(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		displayTmpl(w, "change.html", map[string]interface{}{})
		return
	}

	// Check the two new passwords are identical
	if r.PostFormValue("newpassword") != r.PostFormValue("new2password") {
		displayTmplError(w, http.StatusNotAcceptable, "change.html", map[string]interface{}{"error": "New passwords are not identical. Please retry."})
	} else if len(r.PostFormValue("login")) == 0 {
		displayTmplError(w, http.StatusNotAcceptable, "change.html", map[string]interface{}{"error": "Please provide a valid login"})
	} else if err := checkPasswdConstraint(r.PostFormValue("newpassword")); err != nil {
		displayTmplError(w, http.StatusNotAcceptable, "change.html", map[string]interface{}{"error": "The password you chose doesn't respect all constraints: " + err.Error()})
	} else {
		conn, err := myLDAP.Connect()
		if err != nil || conn == nil {
			log.Println(err)
			displayTmplError(w, http.StatusInternalServerError, "change.html", map[string]interface{}{"error": err.Error()})
		} else if err := conn.ServiceBind(); err != nil {
			log.Println(err)
			displayTmplError(w, http.StatusInternalServerError, "change.html", map[string]interface{}{"error": err.Error()})
		} else if dn, err := conn.SearchDN(r.PostFormValue("login"), true); err != nil {
			log.Println(err)
			displayTmplError(w, http.StatusInternalServerError, "change.html", map[string]interface{}{"error": err.Error()})
		} else if err := conn.Bind(dn, r.PostFormValue("password")); err != nil {
			log.Println(err)
			displayTmplError(w, http.StatusUnauthorized, "change.html", map[string]interface{}{"error": err.Error()})
		} else if err := conn.ChangePassword(dn, r.PostFormValue("newpassword")); err != nil {
			log.Println(err)
			displayTmplError(w, http.StatusInternalServerError, "change.html", map[string]interface{}{"error": err.Error()})
		} else {
			displayMsg(w, "Password successfully changed!", http.StatusOK)
		}
	}
}
