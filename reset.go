package main

import (
	"log"
	"net/http"
	"strings"
)

func resetPassword(w http.ResponseWriter, r *http.Request) {
	if len(r.URL.Query().Get("l")) == 0 || len(r.URL.Query().Get("t")) == 0 {
		http.Redirect(w, r, "lost", http.StatusFound)
	}

	base := map[string]interface{}{
		"login": r.URL.Query().Get("l"),
		"token": strings.Replace(r.URL.Query().Get("t"), " ", "+", -1),
	}

	if r.Method != "POST" {
		displayTmpl(w, "reset.html", base)
		return
	}

	// Check the two new passwords are identical
	if r.PostFormValue("newpassword") != r.PostFormValue("new2password") {
		base["error"] = "New passwords are not identical. Please retry."
		displayTmplError(w, http.StatusNotAcceptable, "reset.html", base)
		return
	} else if err := checkPasswdConstraint(r.PostFormValue("newpassword")); err != nil {
		base["error"] = "The password you chose doesn't respect all constraints: " + err.Error()
		displayTmplError(w, http.StatusNotAcceptable, "reset.html", base)
		return
	}

	// Connect to the LDAP server
	conn, err := myLDAP.Connect()
	if err != nil || conn == nil {
		log.Println(err)
		base["error"] = err.Error()
		displayTmplError(w, http.StatusInternalServerError, "reset.html", base)
		return
	}

	// Bind as service to perform the search
	err = conn.ServiceBind()
	if err != nil {
		log.Println(err)
		base["error"] = err.Error()
		displayTmplError(w, http.StatusInternalServerError, "reset.html", base)
		return
	}

	// Search the dn of the given user
	dn, err := conn.SearchDN(r.PostFormValue("login"), true)
	if err != nil {
		log.Println(err)
		base["error"] = err.Error()
		displayTmplError(w, http.StatusInternalServerError, "reset.html", base)
		return
	}

	// Check token validity (allow current token + last one)
	if conn.genToken(dn, false) != r.PostFormValue("token") && conn.genToken(dn, true) != r.PostFormValue("token") {
		base["error"] = "Token invalid, please retry the lost password procedure. Please note that our token expires after 1 hour."
		displayTmplError(w, http.StatusNotAcceptable, "reset.html", base)
		return
	}

	// Replace the password by the new given
	if err := conn.ChangePassword(dn, r.PostFormValue("newpassword")); err != nil {
		log.Println(err)
		base["error"] = err.Error()
		displayTmplError(w, http.StatusInternalServerError, "reset.html", base)
		return
	}

	displayMsg(w, "Password successfully changed!", http.StatusOK)
}
